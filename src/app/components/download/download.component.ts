import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ares-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {

  public loadContent(): void  {
      var macURL = "https://assets.global.ax/app/v0.1.1/Global.AX-0.1.1.dmg";
      var winURL = "https://assets.global.ax/app/v0.1.1/Global.AX-0.1.1.exe";
      document.getElementById('macDl').setAttribute('href', macURL);
      document.getElementById('winDl').setAttribute('href', winURL);

      var mac = /(Mac|iPhone|iPod|iPad)/i.test(navigator.platform);
     if(mac) {
        setTimeout(function () { window.location.href = macURL; }, 5000)
        document.getElementById('s1').innerHTML="Your app should be downloading... if not, click the button for macOS above";
        document.getElementById('s2').innerHTML='Open the newly downloaded installer <br> <img src="http://i.cammm.pw/ss/1596552746/Screen-Shot-2020-08-04-15-52-24.png"/>';
        document.getElementById('s3').innerHTML='Drag <b>Global</b> to the <b>Applications</b> folder and open it. <br> <img src="http://i.cammm.pw/ss/1596551505/Screen-Shot-2020-08-04-15-31-43.png" style="width:50%;"/>';
        document.getElementById('s4').innerHTML='Open your <b>Applications</b> folder and right click on <b>Global</b> & click <b>open</b> <br> <img src="http://i.cammm.pw/ss/1596551616/Screen-Shot-2020-08-04-15-33-33.png" style="width:50%;"/>';
        document.getElementById('s5').innerHTML='The application will now open, this <b>Must</b> be done to register the app, and allow you to launch the client.<br><b>IT WILL BE A BLANK SCREEN IF NOT OPENED VIA THE WEBSITE</b>'
     } else {
        setTimeout(function () { window.location.href = winURL; }, 5000)
        document.getElementById('s1').innerHTML='Your app should be downloading... if not, click the button for Windows above.';
        document.getElementById('s2').innerHTML='Open the newly downloaded installer <br> <img src="http://i.cammm.pw/ss/brave_rXE8lhStqv.png"/>';
        document.getElementById('s3').innerHTML='Allow the Application through Microsoft Smartscreen, and it will automatically install <br> <img src="http://i.cammm.pw/ss/TtPcIeISKo.png" style="float: left;width: 49%;"/> <img src="http://i.cammm.pw/ss/psmiFQxb1m.png" style="float: right;width: 49%;margin-left: 5px;"/>';
        document.getElementById('s4').innerHTML='The application will now automatically install. <img src="http://i.cammm.pw/ss/Global-0.1.0_ribFfywpFk.png" style="width:100%;"/>';
        document.getElementById('s5').innerHTML='The application will now be open. It <b>Must</b> be opened at least once, to allow you to launch the client. <br> <b>IT WILL BE A BLANK SCREEN IF NOT OPENED VIA THE WEBSITE</b>';
     }
  };
  constructor() { }

  ngOnInit(): void {
    this.loadContent();
  }

}
