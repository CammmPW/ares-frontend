import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DownloadRoutingModule } from './download-routing.module';
import {LayoutModule} from '../layout/layout.module';
import { DownloadComponent } from 'src/app/components/download/download.component';


@NgModule({
  declarations: [DownloadComponent],
    imports: [
        CommonModule,
        DownloadRoutingModule,
        LayoutModule,
    ]
})
export class DownloadModule { }
