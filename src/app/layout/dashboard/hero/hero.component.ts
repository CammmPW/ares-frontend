import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../models/user/user';
import {UserService} from '../../../services/user.service';
import {environment} from '../../../../environments/environment';
import {client} from '../../../../environments/environment';
import {ClientService} from '../../../services/client.service';
import {Subscription} from 'rxjs';
import {LanguageService} from '../../../services/language.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'ares-layout-dashboard-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit, OnDestroy {
  counterSubscription: Subscription;
  ticketSubscription: Subscription;
  counter = 0;
  ticket = "undefined";

  user: User;

  name = environment.app.hotelName;
  date = environment.app.components.dashboard.hero.date;
  time = environment.app.components.dashboard.hero.time;
  lastLogin: number;

  constructor(
    private userService: UserService,
    private clientService: ClientService,
    private languageService: LanguageService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    this.user = this.userService.user;
    this.lastLogin = this.user.last_login * 1000;

    this.counterSubscription = this.clientService.counter().subscribe({
      next: value => {
        this.counter = value;

        if (value > 1) {
          this.name = `${environment.app.hotelName}'s`;
        }
      }
    });

    this.ticketSubscription = this.clientService.ticket().subscribe({
      next: (ticket: string) => {
        client.vars['sso.ticket'] = ticket;
      }
    });
  }
  

  sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  launchApp():void {
    this.clientService.findretros().subscribe({
      next: (findretros_api: string) => {
        if (findretros_api == "VOTED" || window.location.href.indexOf("voted") > -1) {
          console.log(findretros_api);
          this.launchAppHandler();
        } else {
          console.log("window replace" + findretros_api);
          window.location.replace(findretros_api);
        }
      }
    });
}

  launchAppHandler() {
    window.location.href = `habbo://client/?token=${client.vars["sso.ticket"]}&r=0&d=1`;
    
    setTimeout(function() {
      if ( document.hasFocus() ) {
          window.location.href = "/download";
      }else{
          console.log('has no focus');
      }
  }, 3000);
  }

  authTicket(): string {
    return ``;
  }

  figure(): string {
    return `${environment.app.imager}${this.user.look}&size=l`;
  }

  ngOnDestroy() {
    if (this.counterSubscription && !this.counterSubscription.unsubscribe) {
      this.counterSubscription.unsubscribe();
    }
    if (this.ticketSubscription && !this.ticketSubscription.unsubscribe) {
      this.ticketSubscription.unsubscribe();
    }
  }

  get locale(): string {
    return this.languageService.getCurrentCulture();
  }

}
